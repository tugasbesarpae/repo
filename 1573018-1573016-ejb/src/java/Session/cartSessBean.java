/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Session;

import Entity.*;
import javax.ejb.*;
import java.util.*;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Andre
 */
@Stateful
@StatefulTimeout(20000)
@LocalBean
public class cartSessBean implements cartSessRemote{

    @PersistenceContext(unitName = "1573018-prak8-ejbPU")
    private EntityManager em;
    
    private List<book> cartBooks = new ArrayList<>();

    @Override
    public void addItem(book b) {
        if (!cartBooks.contains(b)) {
            cartBooks.add(b);
        }
    }

    @Override
    public void removeItem(book b) {
        if (cartBooks.contains(b)) {
            cartBooks.remove(b);
        }
    }

    @Override
    public Float getTotal() {
        if (cartBooks == null || cartBooks.isEmpty()) {
            return 0f;
        }
        Float total = 0f;
        for (book b : cartBooks) {
            total += (b.getPrice());
        }
        return total;
    }
 
    @Remove
    @Override
    public void checkout() {
        pesanan p = new pesanan();
        detailPesanan dp = new detailPesanan();
        
        List<detailPesanan> listDetail = new ArrayList<>();
        Float totalHarga = 0f;
        for(book b : cartBooks){
            dp.setId(b);
            dp.setHargaBeli(b.getPrice());
            System.out.println("-----------------+++\t\t\t"+p.getIdOrder()+"\n\n\n");
            dp.setIdOrder(p);
            listDetail.add(dp);
            totalHarga+=b.getPrice();
            dp = new detailPesanan();
        }
        
        p.setDaftarDetailPesanan(listDetail);
        p.setTotalHarga(totalHarga);
        em.persist(p);
        
        //apa DP nya juga perlu di loop satu2?
        cartBooks.clear();
    }

    @Remove
    @Override
    public void empty() {
        cartBooks.clear();
    }

    //getter-setter
    public List<book> getCartBooks() {
        return cartBooks;
    }

    public void setCartBooks(List<book> cartBooks) {
        this.cartBooks = cartBooks;
    }

}
