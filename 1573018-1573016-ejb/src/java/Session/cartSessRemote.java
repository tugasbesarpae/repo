/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Session;

import Entity.book;
import javax.ejb.Remote;
import javax.ejb.Remove;

/**
 *
 * @author Andre
 */
@Remote
public interface cartSessRemote {
    public void addItem(book b);
    public void removeItem(book b);

    public Float getTotal();
 
    @Remove
    public void checkout();
    
    @Remove
    public void empty();
}
