/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.*;

/**
 *
 * @author Andre
 */
@Entity
public class pesanan implements Serializable {

    @Id
    @GeneratedValue
    private int idOrder;

    private Float totalHarga;
    
    @ManyToOne
    @JoinColumn(name = "idCustomer")
    private pelanggan idCustomer;

    @OneToMany(mappedBy = "idOrder",
            cascade = CascadeType.ALL)
    private List<detailPesanan> daftarDetailPesanan = new ArrayList<>();

    public pesanan() {
    }

    public pesanan(int idOrder, Float totalHarga) {
        this.idOrder = idOrder;
        this.totalHarga = totalHarga;
    }

    //getter-setter
    public int getIdOrder() {
        return idOrder;
    }

    public void setIdOrder(int idOrder) {
        this.idOrder = idOrder;
    }

    public Float getTotalHarga() {
        return totalHarga;
    }

    public pelanggan getIdCustomer() {
        return idCustomer;
    }

    public void setIdCustomer(pelanggan idCustomer) {
        this.idCustomer = idCustomer;
    }

    public void setTotalHarga(Float totalHarga) {
        this.totalHarga = totalHarga;
    }

    public List<detailPesanan> getDaftarDetailPesanan() {
        return daftarDetailPesanan;
    }

    public void setDaftarDetailPesanan(List<detailPesanan> daftarDetailPesanan) {
        this.daftarDetailPesanan = daftarDetailPesanan;
    }

}
