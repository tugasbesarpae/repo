/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 *
 * @author Andre
 */
@Entity
public class detailPesanan implements Serializable {

    @Id
    @GeneratedValue
    private int idDetailOrder;

    @ManyToOne
    @JoinColumn(name = "idOrder")
    private pesanan idOrder;

    @OneToOne(cascade = CascadeType.ALL)
    private book id;

    private Float hargaBeli;

    public detailPesanan() {
    }

    //getter-setter
    public int getIdDetailOrder() {
        return idDetailOrder;
    }

    public void setIdDetailOrder(int idDetailOrder) {
        this.idDetailOrder = idDetailOrder;
    }

    public pesanan getIdOrder() {
        return idOrder;
    }

    public void setIdOrder(pesanan idOrder) {
        this.idOrder = idOrder;
    }

    public book getId() {
        return id;
    }

    public void setId(book id) {
        this.id = id;
    }

    public Float getHargaBeli() {
        return hargaBeli;
    }

    public void setHargaBeli(Float hargaBeli) {
        this.hargaBeli = hargaBeli;
    }

}
